#include <stdio.h> /* printf, sprintf */
#include <string.h>
#include <stdlib.h> /* exit */
#include <unistd.h> /* read, write, close */
#include <string.h> /* memcpy, memset */
#include <sys/socket.h> /* socket, connect */
#include <netinet/in.h> /* struct sockaddr_in, struct sockaddr */
#include <netdb.h> /* struct hostent, gethostbyname */

const char host[20] = "www.iu3.bmstu.ru";
const char webapi[16] = "/WebApi/time";
const int portno = 8090;

#define h_addr h_addr_list[0]

void error(const char *msg) { perror(msg); exit(0); }


int main(int argc,char *argv[])
{
    /* first what are we going to send and where are we going to send it? */

    if(argc < 5){
        fprintf(stderr, "Usage: %s -t <type> -f <format>\n", argv[0]);
	fprintf(stderr, "Usage: %s -t <type> -f <format> -post\n", argv[0]);
	fprintf(stderr, "type: uct or local\nformat: unix or internet\n");
        exit(1);
    }

char message_fmt[1024];

if (argc > 5){
//POST /WebApi/time HTTP/1.0\nContent-Type:application/x-www-form-urlencoded\nContent-Length:20\n\ntype=utc&format=unix\n
	strcpy (message_fmt,"POST ");
	strcat(message_fmt, webapi);
	strcat(message_fmt, " HTTP/1.0\nContent-Type:application/x-www-form-urlencoded\nContent-Length:20\n\ntype=");
	strcat(message_fmt,argv[2]);
	strcat(message_fmt,"&format=");
        strcat(message_fmt,argv[4]);
	strcat(message_fmt,"\n");
}
else{
//GET /WebApi/time?type=local&format=unix HTTP/1.0\n\n
    	strcpy (message_fmt,"GET ");
    	strcat(message_fmt, webapi);
    	strcat(message_fmt,"?type=");
	strcat(message_fmt,argv[2]);
	strcat(message_fmt,"&format=");
        strcat(message_fmt,argv[4]);
	strcat(message_fmt," HTTP/1.0\n\n");
}

    struct hostent *server;
    struct sockaddr_in serv_addr;
    int sockfd, bytes, sent, received, total;
    char message[1024],response[4096];

    /* fill in the parameters */
    sprintf(message,message_fmt,argv[1],argv[2]);
    printf("Request:\n%s\n",message);

    /* create the socket */
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) error("ERROR opening socket");

    /* lookup the ip address */
    server = gethostbyname(host);
    if (server == NULL) error("ERROR, no such host");

    /* fill in the structure */
    memset(&serv_addr,0,sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(portno);
    memcpy(&serv_addr.sin_addr.s_addr,server->h_addr,server->h_length);

    /* connect the socket */
    if (connect(sockfd,(struct sockaddr *)&serv_addr,sizeof(serv_addr)) < 0)
        error("ERROR connecting");

    /* send the request */
    total = strlen(message);
    sent = 0;
    do {
        bytes = write(sockfd,message+sent,total-sent);
        if (bytes < 0)
            error("ERROR writing message to socket");
        if (bytes == 0)
            break;
        sent+=bytes;
    } while (sent < total);

    /* receive the response */
    memset(response,0,sizeof(response));
    total = sizeof(response)-1;
    received = 0;
    do {
        bytes = read(sockfd,response+received,total-received);
        if (bytes < 0)
            error("ERROR reading response from socket");
        if (bytes == 0)
            break;
        received+=bytes;
    } while (received < total);

    if (received == total)
        error("ERROR storing complete response from socket");

    /* close the socket */
    close(sockfd);

    /* process response */
    printf("Response:\n%s\n",response);

    return 0;
}
