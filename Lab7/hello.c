/* hello.c – Заготовка для второй лабораторной работы */
#include <linux/module.h>
#include <linux/version.h>
#include <linux/kernel.h>
#include <linux/device.h>
#include <linux/cdev.h>
#include <linux/fs.h>
#include <asm/uaccess.h> 
#include <linux/string.h>
#include <linux/netfilter.h>
#include <linux/netfilter_ipv4.h>
#include <linux/ip.h>
#include <linux/tcp.h>
#include <linux/udp.h>

static dev_t dev;
static struct cdev c_dev;
static struct class * cl;

static int my_open(struct inode *inode, struct file *file);
static int my_close(struct inode *inode, struct file *file);
static ssize_t my_read(struct file *filp, char *buffer, size_t length, loff_t * offset);
static ssize_t my_write(struct file *filp, const char *buff, size_t len, loff_t * off);

static struct file_operations hello_fops =
{
	.owner = THIS_MODULE,
	.open = my_open,
	.release = my_close,
	.read = my_read,
	.write = my_write
};

static int __init hello_init(void) /* Инициализация */
{
	int retval;
	bool allocated = false;
	bool created = false;
	cl = NULL;

	retval = alloc_chrdev_region(&dev, 0, 1, "hello");
	if (retval)
		goto err;

	allocated = true;
	printk(KERN_INFO "Major number = %d Minor number = %d\n", MAJOR(dev), MINOR(dev));
	cl = class_create(THIS_MODULE, "teach_devices");
	if (!cl) {
		retval = -1;
		goto err;
	}

	if (device_create(cl, NULL, dev, NULL, "hello") == NULL)
	{
		retval = -1;
		goto err;
	}

	created = true;
	cdev_init(&c_dev, &hello_fops);
	retval = cdev_add(&c_dev, dev, 1);
	if (retval)
		goto err;

	printk(KERN_INFO "Hello: regisered");
	return 0;

err:
	printk("Hello: initialization failed with code %08x\n", retval);
	if (created)
		device_destroy(cl, dev);

	if (allocated)
		unregister_chrdev_region(dev, 1);

	if (cl)
		class_destroy(cl);

	return retval;
}

#define BUFFER_SIZE 1024
#define BUFFER_MASK BUFFER_SIZE - 1

char roundBuffer[BUFFER_SIZE]; /* Кольцевой буфер */
int read = 0, write = 0;

/* Запись символа в буфер */
void bufferWrite (const char symbol)
{
    roundBuffer[write++ & BUFFER_MASK] = symbol;
}

/* Чтение символа из буфера */
const char bufferRead (void)
{
    return roundBuffer[read++ & BUFFER_MASK];
}


/* Структура, которая используется для регистрации обработчика. */
static struct nf_hook_ops nfho;

/* Запись протокола */
void writeProtocol(__u8 protocol)
{
    printk(KERN_INFO "Write protocol: %d\n", protocol);

    if(protocol == IPPROTO_UDP)
    {
        bufferWrite('U');
        bufferWrite('D');
        bufferWrite('P');
    }
    else if (protocol == IPPROTO_TCP)
    {
        bufferWrite('T');
        bufferWrite('C');
        bufferWrite('P');
    }
    else if (protocol == IPPROTO_ICMP)
    {
        bufferWrite('I');
        bufferWrite('C');
        bufferWrite('M');
        bufferWrite('P');
    }
}

/* Запись IPv4-адреса */
void writeAddress(__be32* address)
{
    unsigned char* i = (unsigned char*) address;
    for(; i < (unsigned char*) (address + 1); i++)
    {
        printk(KERN_INFO "Write address: %d\n", *i);

        char number[4];
        sprintf(number, "%d", *i);

        char* j = number;
        for(; *j != '\0'; j++)
            bufferWrite(*j);
        bufferWrite('.');
    }
}

/* Запись номера порта */
void writePort(__be16 port)
{
    printk(KERN_INFO "Write port: %d\n", port);

    char number[10];
    sprintf(number, "%d", port);

    char *i = number;
    for(; *i != '\0'; i++)
    {
         bufferWrite(*i);
    }
}

/* Собственно функция обработчика. */
unsigned int hook_func(void *ops,
			       struct sk_buff *skb,
			       const struct nf_hook_state *state)
{
    struct iphdr* ip = ip_hdr(skb);

    bufferWrite('P'); 
    bufferWrite('a');
    bufferWrite('c');
    bufferWrite('k');
    bufferWrite('e');
    bufferWrite('t');
    bufferWrite('!');
    bufferWrite(' ');

    writeProtocol(ip->protocol);
    bufferWrite(' '); 

    writeAddress(&ip -> saddr);
    bufferWrite(':'); 

    __be16* hdr = (__be16*)(skb->data + ip->ihl * 4);

    writePort(*hdr);
    bufferWrite('-');
    bufferWrite('-');
    bufferWrite('-');
    bufferWrite('>'); 

    writeAddress(&ip -> daddr);
    bufferWrite(':');
 
    writePort(*(++hdr));
    bufferWrite('\n'); 
  
    return NF_ACCEPT;          
}

 struct net* network; /*Создаем новую сетевую структуру */

/* Процедура регистрирует обработчик сетевых пакетов */
int ip_handler_register(void)
{
	/* Fill in our hook structure */
	nfho.hook     = (nf_hookfn*) hook_func;
	/* Handler function */
	nfho.hooknum  = NF_INET_PRE_ROUTING; /* Первое сетевое событие для IPv4 */
	nfho.pf       = PF_INET;
	nfho.priority = NF_IP_PRI_FIRST;   /* Назначаем обработчику 1-ый приоритет */

    for_each_net(network)
	    nf_register_net_hook(network, &nfho);

	printk("Hello: Netfilter hook registered!\n");
	return 0;
}

/* Процедура отменяет регистрацию обработчика сетевых пакетов */
void ip_handler_unregister(void)
{
    nf_unregister_net_hook(network, &nfho);
	printk("Hello: Netfilter hook unregistered!\n");
}

static int my_open(struct inode *inode, struct file *file)
{
    printk("Hello: open.\n");
    ip_handler_register();
	return 0;
}

static int my_close(struct inode *inode, struct file *file)
{
    printk("Hello: close.\n");
    ip_handler_unregister();
	return 0;
}

static ssize_t my_read(struct file *filp,  
                           char *buffer, /* буфер данных */
                           size_t length, /* длина буфера */
                           loff_t * offset)
{
	char symbol = '\0';

    if(read != write)
        symbol = bufferRead();

    if (raw_copy_to_user(buffer, &symbol, 1))
		return -EFAULT;

	return 1; /* количество байт возвращаемых драйвером в буфере */
}

static ssize_t my_write(struct file *filp, const char *buff, size_t len, loff_t * off)
{
        return -EINVAL;
}


static void __exit hello_exit(void) /* Деинициализация */
{
    printk(KERN_INFO "Hello: unregistered\n");
    device_destroy (cl, dev);
    unregister_chrdev_region (dev, 1);
    class_destroy (cl);
}

module_init(hello_init);
module_exit(hello_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Ivan Sidyakin");
MODULE_DESCRIPTION("Simple loadable kernel module");
