#include <stdio.h>
#include <fcntl.h>
#include <string.h>

#define device_name "/dev/hello"
#define file_name "/usr/share/misc/pci.ids"

#define BUFFER_SIZE 1024 // размер буфера
char buffer[BUFFER_SIZE];
FILE *f;

/* Вывод информации о продавце */
void printVendorInformation(const unsigned short vendor)
{
    char str[250];
    while (fgets(str, 100, f)) /* помещаем 99 символов из файла f в массив str*/
    {
        if(str[0] != '#' && str[0] != '\t')
        {
            unsigned short newVendor;
            char vendorStroka[100];
            sscanf(str, "%hx %[^\t\n]", &newVendor, vendorStroka);

            if(newVendor == vendor)
            {
                printf("%s \t", vendorStroka);
                return;
            }
        }
    }

    printf("Vendor was not found!!!");
}

/* Вывод информации об устройстве */
void printDeviceInformation(const unsigned short device)
{
    char str[250];
    while (fgets(str, 100, f))
    {
        if(str[0] == '\t' && str[1] != '\t')
        {
            unsigned short newDevice;
            char deviceStroka[100];
            sscanf(str, "%hx %[^\t\n]", &newDevice, deviceStroka);

            if(newDevice == device)
            {
                printf("%s \t\t", deviceStroka);
                return;
            }
        }
    }

    printf("Device was not found!!!");
}

/* Вывод информации об устройствах, подключенных к шине PCI. */
void print_pci_information(void)
{
    char* token;
    char* separator = "\n";
    token = strtok(buffer, separator);//Разбивает строку на части по указанному разделителю separator

    while(token)
    {
        unsigned short vendor;
        unsigned short device; 
        char address[100]; //масcив для полного адреса устройства в текстовом виде

        /* Считывание данных из массива token*/
        sscanf(token, "%hu %hu %s", &vendor, &device, address);
        printVendorInformation(vendor);
        printDeviceInformation(device);

        fseek(f, 0, SEEK_SET);  /* Устанавливаем указатель положения в файле на его начало. */

        printf("%s\n\n", address);
        token = strtok(NULL, separator);
    }

}


int main(int argc, char* argv)
{
    int dev = open(device_name, O_RDWR);

    if(dev == -1)
    {
        printf("Device opening error\n");
        return -1;
    }


    if(ioctl(dev, 0, buffer))
    {
        printf("DEvice read error\n");
        return -1;
    }

    close(dev);

    f = fopen(file_name, "r");

    if(!f)
    {
        printf("File opening error\n");
        return -1;
    }

    print_pci_information();

    fclose(f);

    return 0;
}

