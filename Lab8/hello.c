/* hello.c – Заготовка для второй лабораторной работы */
#include <linux/module.h>
#include <linux/version.h>
#include <linux/kernel.h>
#include <linux/device.h>
#include <linux/cdev.h>
#include <linux/fs.h>
#include <asm/uaccess.h> 
#include <linux/pci.h>	

static dev_t dev;
static struct cdev c_dev;
static struct class * cl;

static int my_open(struct inode *inode, struct file *file);
static int my_close(struct inode *inode, struct file *file);
static long pci_ioctl(struct file *f, unsigned int cmd, unsigned long arg);


static struct file_operations hello_fops =
{
	.owner = THIS_MODULE,
	.open = my_open,
	.release = my_close,
	.unlocked_ioctl = pci_ioctl
};

static int __init hello_init(void) /* Инициализация */
{
	int retval;
	bool allocated = false;
	bool created = false;
	cl = NULL;

	retval = alloc_chrdev_region(&dev, 0, 1, "hello");
	if (retval)
		goto err;

	allocated = true;
	printk(KERN_INFO "Major number = %d Minor number = %d\n", MAJOR(dev), MINOR(dev));
	cl = class_create(THIS_MODULE, "teach_devices");
	if (!cl) {
		retval = -1;
		goto err;
	}

	if (device_create(cl, NULL, dev, NULL, "hello") == NULL)
	{
		retval = -1;
		goto err;
	}

	created = true;
	cdev_init(&c_dev, &hello_fops);
	retval = cdev_add(&c_dev, dev, 1);
	if (retval)
		goto err;

	printk(KERN_INFO "Hello: regisered");
	return 0;

err:
	printk("Hello: initialization failed with code %08x\n", retval);
	if (created)
		device_destroy(cl, dev);

	if (allocated)
		unregister_chrdev_region(dev, 1);

	if (cl)
		class_destroy(cl);

	return retval;
}

#define BUFFER_SIZE 1024 // размер буфера
char buffer[BUFFER_SIZE];
int devPos = 0;

void find_devices(void)
{
	struct pci_dev* pdev = NULL;
	
	for_each_pci_dev(pdev) // цикл по всем устройствам
	{
		/* Полный адрес устройства на шине в текстовом виде */
		const char* name = pci_name(pdev);
        const int size = BUFFER_SIZE - devPos; // получение одлину строки, которую можно записать в devPos

        if(size <= 1)
            return;

        /*
            Заносим в массив buffer с позиции devPos с ограничением по длине в size следующие данные: продавец, устройство, 
            полный адрес устройства на шине в текстовом виде в пользовательский буфер.
            И изменяем позицию следующего элемента массива buffer.
        */
        devPos += snprintf(buffer + devPos, size, "%hu %hu %s", pdev -> vendor, pdev -> device, name);

		buffer[devPos++] = '\n'; /* Завершаем запись данных об устройстве, добавляя в следующий элемент массива символ конца строки. */
	}
}

static int my_open(struct inode *inode, struct file *file)
{
    find_devices();
	return 0;
}

static int my_close(struct inode *inode, struct file *file)
{
	return 0;
}

static long pci_ioctl(struct file *f, unsigned int cmd, unsigned long arg) /* Запись массива buffer в пользовательский буфер. */
{
    if (raw_copy_to_user((char*) arg, buffer, devPos))
		return -EACCES;	
 
	return 0;
}


static void __exit hello_exit(void) /* Деинициализаия */
{
    printk(KERN_INFO "Hello: unregistered\n");
    device_destroy (cl, dev);
    unregister_chrdev_region (dev, 1);
    class_destroy (cl);
}

module_init(hello_init);
module_exit(hello_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Ivan Sidyakin");
MODULE_DESCRIPTION("Simple loadable kernel module");
