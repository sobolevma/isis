/*
 *  Задание #6
 *  Автор: 
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "map.h"

void undecorate_name(char * name);
int main(int argc, char * argv[])
{
    COUNTRY ** map;
    
    char cmd[10];
    char name[60], populn[20], terr[20];
    


    map = map_load();

    while(strcmp(cmd, "quit") != 0){
        //Ввод команды
        printf("\nВведите команду >>");
        scanf("%s", cmd);
        if (strcmp(cmd, "add") == 0) //  strcmp - функция сравнения строк
        { 
            char *end;
            
            printf("Введите название страны >>");
            scanf("%s", name);
            undecorate_name(name); //Заменяем сиволы _ на пробелы

            printf("Введите количество населения страны >>");                
            scanf("%s",populn);
            int population = (int) strtol(populn, &end, 10);//конвертация строки в число  
            
            printf("Введите площадь страны в км^2 >>");             
	        scanf("%s", terr);            
            int area = (int) strtol(terr, &end, 10);//конвертация строки в число  
            
            //Добавление страны в хэш-таблицу            
           map_add(map, name, population, area);              
        }  
        else if ( strcmp(cmd, "delete") == 0) 
        { 
            printf("Введите название страны >>");
            scanf("%s", name);
            undecorate_name(name);

            //Удаление страны из хэш-таблицы           
            map_delete(map, name);   
                                   
        }
        else if ( strcmp(cmd, "view") == 0) 
        { 
            printf("Введите название страны >>");
            scanf("%s", name);
            undecorate_name(name);
            COUNTRY *contry = map_find(map, name);
                    
            if (contry != NULL) {
                    print_country(contry);
            }
                
        }
        else if ( strcmp(cmd, "dump") == 0) 
        {          
		    map_dump(map);
        }     
        else if(strcmp(cmd, "save") == 0)
        {       
		    map_save(map);
        } 
    }

       
    /* Удаление списка из динамической памяти */      
	map_clear(map);

    return 0;
}

// Функция замены символа _ на символ пробела
void undecorate_name(char * name)
{
    int cnt;
    while (name[cnt]) {
        if (name[cnt] == '_')
            name[cnt] = ' ';
        cnt++;
    }
}
