#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "list.h"

/* Тип указателя на функцию для сравнения элементов списка */
typedef int (* CMP_FUNC)(COUNTRY * v1, COUNTRY * v2);

// Объявление необходимых констант
#define MAX_LINE_LENGTH  256
#define DBNAME "country_database.csv"

/*
 * Выделяет динамическую память для нового элемента списка list,
 * инициализирует поля и добавляет элемент в начало списка
 * Изменяет указатель на начало списка. Возвращает 0 если новый элемент
 * добавлен или 1 если не удалось выделить память для элемента. 
 */
int add( COUNTRY ** list, char * name, int population, int area)
{
    COUNTRY * contr = malloc(sizeof(COUNTRY));//contr - новый элемент, который будет добавлен в БД
    if(contr == NULL){
            return 1;
    }
    
    strncpy(contr -> name, name, sizeof(contr -> name));
    contr->population = population; 
    contr->area = area;        
    contr->next = *list;
    *list = contr; 
    
    return 0; /* ok */  
}


/* Удаляет элемент списка list и освобождает выделенную по него память.
 *   Если элемент первый, изменяет указатель на список 
 */
void _delete(COUNTRY ** list, COUNTRY * v)
{
     if (list == NULL || *list == NULL || v == NULL) {
        return;
    }

    /*Для случая первого элемента*/
    if (compare_name(*list, v) == 0) {
        COUNTRY* con = *list;
        if ((*list) -> next != NULL) {
            (*list) -> next = (*list) -> next -> next;
            *list = (*list) -> next;
        } else {
            *list = NULL;
        }
        free(con);
        return;
    }
    
    /*Для всех остальных случаев*/
    for (COUNTRY* contr = (*list); contr -> next != NULL; contr = contr -> next) {
        if (compare_name(contr -> next, v) == 0) {
            COUNTRY* del_contr = contr -> next;
            contr -> next 		= contr -> next -> next;
            free(del_contr);
            return;
        }
    }
    
}


/*
 * Ищет в списке страну с заданным именем name. Возвращает указатель на
 * найденный элеемнт списка или NULL, если страна с таким названием отсутсвует
 * в списке.
 */
COUNTRY * find(COUNTRY * list, char * name)
{    
    COUNTRY *con = NULL, *p = NULL;
    for (con = list; con!=NULL; con = con->next) {
        if (strcmp(con->name, name) == 0) {                       
            p = con;
            break;
        }
    }

    return p;
}


/*
 * Возвращает количество элементов в списке 
 */
int count(COUNTRY * list)
{
    int cnt = 0;    

    //Количество элементов в массиве
    for (COUNTRY* contr_list = list; contr_list != NULL; contr_list = contr_list -> next) {
        cnt++;
    }    
    return cnt;
}


/* Быстрая сортировка массива указателей на элементы списка */
void quick_sort(COUNTRY ** ca, int first, int last, CMP_FUNC compare)
{
    int i, j;
    COUNTRY* v;
    COUNTRY* p;
    i = first;
    j = last;
    v = ca[(first + last) / 2];

    do {
        while (compare(ca[i], v) < 0) i++;
        while (compare(ca[j], v) > 0) j--;

        if (i <= j) {
            if (compare(ca[i], ca[j]) > 0) {
                p 		= ca[j];
                ca[j] 	= ca[i];
                ca[i] 	= p;
            }
            i++;
            j--;
        }
    } while (i <= j);

    if (i < last)
        quick_sort(ca, i, last, compare);

    if (first < j)
        quick_sort(ca, first, j, compare);
}

//Функции сравнения
int compare_name(COUNTRY *v1, COUNTRY *v2)
{
    return strcmp(v1->name, v2->name);
}

int compare_area(COUNTRY *v1, COUNTRY *v2)
{
     
    int val = (v1->area == v2->area)?0:((v1->area > v2->area)?1:-1);
    return val;
}

int compare_population(COUNTRY *v1, COUNTRY *v2)
{
    
    int val = (v1->population == v2->population)?0:((v1->population > v2->population)?1:-1);
    return val; 
}

/* 
 * Сортирует список по именам стран в алфавитном порядке методом пузырьковой 
 * сортировки. Указатель на начало списка может измениться в результате   
 * сортировки 
 */
int sort(COUNTRY ** list, CMP_FUNC compare)
{

    int cnt, i;
    COUNTRY *p, **ca = NULL;

    cnt = count(*list);
    if (cnt < 2)
        return 0;

    /* выделяем память под массив указателей */
    ca = (COUNTRY**) malloc(cnt* sizeof(COUNTRY*));
    if (!ca)	
        return 1;

    /* заполняем массив указателями на элементы списка */
    ca[0] = *list;
    for (i = 1; i < cnt; i++)
        ca[i] = ca[i - 1]->next;

    quick_sort(ca, 0, cnt - 1, compare);

    /*заполняем список элементами из отсортированного массива*/
    *list = NULL;
    while (cnt > 0) {
        ca[cnt - 1]->next = *list;
        *list = ca[cnt - 1];
        cnt--;
    }
    free(ca);
    return 0;
}

// Функции сортировки
int sort_by_name(COUNTRY ** list)
{
    if (list == NULL || *list == NULL) {
        return 1;
    }    
    return sort(list, compare_name);
}

int sort_by_area(COUNTRY ** list)
{
    if (list == NULL || *list == NULL) {
        return 1;
    }
    return sort(list, compare_area);
}

int sort_by_population(COUNTRY ** list)
{
    if (list == NULL || *list == NULL) {
        return 1;
    }
    return sort(list, compare_population);
}

void print_country(COUNTRY * p)
{
   
    if (p == NULL) {
        return;
    }

    if(p->population == 0 && p->area == 0)
        printf("%s, Не указано, Не указано \n",p->name);//Выводит название
    else if(p->population == 0)
        printf("%s, Не указано, %d \n",p->name, p->area);//Выводит название и площадь территории
    else if(p->area == 0)
        printf("%s, %d, Не указано\n",p->name, p->population);//Выводит название и количество населения
    else printf("%s, %d, %d \n",p->name, p->population, p->area);//Выводит название все поля
}

void dump(COUNTRY * list)
{
    COUNTRY * p;
    p = list; /* Начало списка */
    while (p != NULL) {
        print_country(p);
        p = p->next;
    }
}

/*
 * удаление всех элементов списка и
 * освобождение выделенной под эти элементы памяти 
 */
void clear(COUNTRY * list)
{
   COUNTRY* del_contr = list;
    while (del_contr != NULL) {
        COUNTRY* con  = del_contr;
        del_contr = del_contr -> next;
        free(con);
    }
    
}

/* Загружает список стран из файла */
COUNTRY * load()
{
    char buf[MAX_LINE_LENGTH + 1];
    char * par[3];
    int cnt, pcount = 0;
    COUNTRY *p, * list = NULL;
    FILE * f = fopen(DBNAME, "r");

    buf[MAX_LINE_LENGTH] = 0x00; 
 
    if (f) {
        while(fgets(buf, MAX_LINE_LENGTH, f)) {
            pcount = 0;
            par[pcount++] = buf;
            cnt = 0;
            while(buf[cnt]) {
                if (buf[cnt] == ',') {
                    buf[cnt] = 0x00;
                    par[pcount++] = &buf[cnt + 1];
                }
                cnt++;
            }
            if (pcount == 3) {
                add(&list, par[0], atoi(par[1]), atoi(par[2])); 
            }           
        }
        fclose(f);
    }
    return list;
}

/* Сохраняет список стран в файл */
void save(COUNTRY * list)
{
    FILE * f = fopen(DBNAME, "w+");
    COUNTRY * p = list;

    if (f) {
        while (p != NULL) {
            fprintf(f, "%s,%d,%d\n", p->name, p->population, p->area);
            p = p->next;
        }

        fclose(f);
    }
}


