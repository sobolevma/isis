/*
 *  Задание #6
 *  Автор: 
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "list.h"

int main(int argc, char * argv[])
{
    COUNTRY * list;

    /* Загрузка списка */
    list = load();

   
    if (argc > 1){
        if (strcmp(argv[1], "add") == 0) //  strcmp - функция сравнения строк
            { 
                if (argc == 5) {
                    char *end;
                   
                    int population = (int) strtol(argv[3], &end, 10);//конвертация строки в число
                    int area = (int) strtol(argv[4], &end, 10);
                    
                    add(&list, argv[2], population, area);

                     /* Сохраняем список */  
                    save(list);                    
                }
                else printf("Неверное количество аргументов.");
            }
        else if ( strcmp(argv[1], "delete") == 0) 
            { 
                if(argc == 3)
                {
                    delete(&list, find(list, argv[2]));                 
                }  

                /* Сохраняем список */  
                save(list);          
            }
        else if ( strcmp(argv[1], "view") == 0) 
            { 
                if (argc == 3) {
                    COUNTRY *contry = find(list, argv[2]);
                    
                    if (contry != NULL) {
                            print_country(contry);
                        }
                }
                else printf("Неверное количество аргументов.");
            }
        else if ( strcmp(argv[1], "count") == 0) 
            { 
                if(argc == 2)
                {
                    printf("%d\n",count(list));
                }
                else printf("Неверное количество аргументов.");
            }     
       else if ( strcmp(argv[1], "dump") == 0) 
            { 
                if (argc == 2) {
                    dump(list);
                }
                else if(argc == 3)
                {
                    if(strcmp(argv[2], "-n") == 0)
                        sort_by_name(&list);
                    else if(strcmp(argv[2], "-a") == 0)
                        sort_by_area(&list);
                    else if(strcmp(argv[2], "-p") == 0)
                        sort_by_population(&list);
                    dump(list);
                }
                else printf("Неверное количество аргументов.");
           
        } 
    }

       
    /* Удаление списка из динамической памяти */    
    clear(list);

    return 0;
}
