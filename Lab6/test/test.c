#include <stdio.h>
#include <fcntl.h>
#include "../hello.h"
#include <sys/stat.h>
#include <time.h>

#define version 1
#define driver_name "/dev/hello"

int f;

int reset(void)
{
    int error = ioctl(f, LEDCMD_RESET_STATE);
    if(error != 0)
        printf("reset: error\n");
    return error;
}

int setState(unsigned char pin, unsigned char value)
{
    struct led_t led;
    led.pin = pin;
    led.value = value;
    int error = ioctl(f, LEDCMD_SET_LED_STATE, &led);    
    if(error != 0)
        printf("setState: error\n");
    return error;
}


int ledState0 (void){
    unsigned char state;
    int error = ioctl(f, LEDCMD_GET_STATE, &state);
    if(error != 0)
        printf("ledState: error\n");
    else
        {
            for(int i = 0; i < 8;i++)
            {
                unsigned char value = (state & (128 >> i)) != 0;
                printf("Value of diode %d is %d. \n", i, value);
            }
                
        }
    return error;
}


int ledState1 (unsigned char pin){
    struct led_t led;    
    led.pin = pin; 
    int error = ioctl(f, LEDCMD_GET_LED_STATE, &led);
    if(error != 0)
        printf("ledState: error\n");
    else
        printf("Value of diode %d is %d.\n", pin, led.value);
    return error;
}


int main(int argc, char * argv[])
{
 
     f = open(driver_name, O_RDWR); /* получаем ссылку на устройство */

     if (f == -1) {
	        printf("Device opening error \n");
            return -1; 
	    }
	    
     int error = 0;
     if(argc == 1)
     {
        printf("format: test <command> <args>\n");
        error = 1; 
        return error;
     }

     char* command = argv[1];
     if (!strcmp(command, "reset"))
     {
         error = reset();
     }
     else if (!strcmp(command, "ledstate"))
     {
        if (argc == 2)
        {
            error = ledState0();
        }
        else
        {
            error = ledState1(atoi(argv[2]));
        }
     }
     else if (!strcmp(command, "on"))
     {
        if (argc < 3)
        {
            printf("Too few arguments in command.\n");
            error = 1;
        }
        else
        {
            error = setState(atoi(argv[2]), 1);
        }
     }
     else if (!strcmp(command, "off"))
     {
        if (argc < 3)
        {
            printf("Too few arguments in command.\n");
            error = 1;
        }
        else
        {
            error = setState(atoi(argv[2]), 0);
        }
     }
      else if (!strcmp(command, "ver"))
     {
        /* Получение даты создания файла. */        
        struct stat statistics;
        stat(driver_name, &statistics);
        time_t timer = statistics.st_mtime;
        struct tm *u = localtime(&timer);
   
        printf ("hello v.%d %04d-%02d-%02d\n", version, u -> tm_year + 1900, u -> tm_mon, u -> tm_mday);
        
     }
     else
     {
        printf("Command is unrecognized.\n");
        printf("Please choose one of the following commands:\nreset, ledstate, ledstate <diode_number>, \non <diode_number>, off <diode_number>, ver.\n\n");
        error = 1;
     }

     return error;
 
}
