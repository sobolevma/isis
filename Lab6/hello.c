/* hello.c – Заготовка для второй лабораторной работы */
#include <linux/module.h>
#include <linux/version.h>
#include <linux/kernel.h>
#include <linux/device.h>
#include <linux/cdev.h>
#include <linux/fs.h>
#include <asm/uaccess.h> 
#include "hello.h"
#include <linux/sched/signal.h>

#define PORT_EMULATION
 
#ifdef PORT_EMULATION
#define inb(port) prn_data_em                                   /* Эмуляция принтерного порта*/
#define outb(data, port) prn_data_em = ((unsigned char)data);
unsigned char prn_data_em;
#endif


#define PARALLEL_PORT 0x378 /* Задаем значение параллельному порту */
#define INITIAL_STATE 0x00 /* Начальное состояние */

static dev_t dev;
static struct cdev c_dev;
static struct class * cl;

static int my_open(struct inode *inode, struct file *file);
static int my_close(struct inode *inode, struct file *file);
static long hello_ioctl(struct file *f, unsigned int cmd, unsigned long arg);



static struct file_operations hello_fops =
{
	.owner = THIS_MODULE,
	.open = my_open,
	.release = my_close,
	.unlocked_ioctl = hello_ioctl,
};

static int __init hello_init(void) /* Инициализация */
{
	int retval;
	bool allocated = false;
	bool created = false;
	cl = NULL;

	retval = alloc_chrdev_region(&dev, 0, 1, "hello");
	if (retval)
		goto err;

	allocated = true;
	printk(KERN_INFO "Major number = %d Minor number = %d\n", MAJOR(dev), MINOR(dev));
	cl = class_create(THIS_MODULE, "teach_devices");
	if (!cl) {
		retval = -1;
		goto err;
	}

	if (device_create(cl, NULL, dev, NULL, "hello") == NULL)
	{
		retval = -1;
		goto err;
	}

	created = true;
	cdev_init(&c_dev, &hello_fops);
	retval = cdev_add(&c_dev, dev, 1);
	if (retval)
		goto err;

	printk(KERN_INFO "Hello: regisered");
	return 0;

 err:
	printk("Hello: initialization failed with code %08x\n", retval);
	if (created)
		device_destroy(cl, dev);

	if (allocated)
		unregister_chrdev_region(dev, 1);

	if (cl)
		class_destroy(cl);

	return retval;
}


static int my_open(struct inode *inode, struct file *file)
{    
	return 0;
}


static int my_close(struct inode *inode, struct file *file)
{
	return 0;
}


static long hello_ioctl(struct file *f, unsigned int cmd, unsigned long arg) /* Обработка переданного файла, команды и аргументов из пользовательского буфера. */
{
    int retval = -EINVAL;
	struct led_t led;
    unsigned char some_state, sval;

	
    switch (cmd){
        /* Обработка команды чтения состояния светодиода */
        case LEDCMD_GET_LED_STATE: {
	        if (raw_copy_from_user(&led, (struct led_t *)arg, sizeof(struct led_t))) /* копируем параметры из пользовательского буфера в локальную переменную */
			    retval = -EACCES;
		    else if ((led.pin < 0) || (led.pin > 7)) /* Номер светодиода должен быть в диапазоне от 0 до 7 */
			    retval = -EINVAL;
		    else 
		    {
			    led.value = (inb(PARALLEL_PORT) & (128 >> led.pin)) != 0;     /* Считываем состояния разрядов параллельного порта и сдвигаем    */
			    if(raw_copy_to_user((struct led_t *)arg, &led, sizeof(struct led_t)))  /*  вправо на led.pin разрядов и маскируем 1. Считаем, что светодиод 0 подключен */ 
				    retval = -EACCES;                                /* к младшему разряду порта.  Затем копируем всю структуру обратно в пользовательский буфер */
			    else
			        retval = 0;
		    }
            break;
	    }

	    /* Обработка команды чтения состояния всех светодиодов */
	    case LEDCMD_GET_STATE: {
	      printk(KERN_INFO "Hello: Get state\n");
          some_state = inb(PARALLEL_PORT);      /* Считываем состояния разрядов параллельного порта. */
          if(raw_copy_to_user((unsigned char *)arg, &some_state, sizeof(unsigned char))) /* Копируем обратно только все состояния, используемых диодов в пользовательский буфер. */
            retval = -EACCES; 
          else
            retval = 0;
          break;
	    }

        /* Обработка команды сброса состояния всех светодиодов в начальное*/
        case LEDCMD_RESET_STATE: {
	      printk(KERN_INFO "Hello: Reset state\n");
          outb(INITIAL_STATE, PARALLEL_PORT); /* Запись в принтерный порт начального состояния. */
          retval = 0;
          break;
	    }

        /* Обработка команды установки состояния определенному светодиоду */
        case LEDCMD_SET_LED_STATE: {
	      if (raw_copy_from_user(&led, (struct led_t *)arg, sizeof(struct led_t))) /* копируем параметры из пользовательского буфера в локальную переменную */
	           retval = -EACCES;
          else if(led.pin < 0 || led.pin > 7) /* Номер светодиода должен быть в диапазоне от 0 до 7 */
               retval = -EINVAL;
          else 
            {
                some_state = inb(PARALLEL_PORT);  /* Считываем состояния разрядов параллельного порта. */
                if (led.value == 0)  /* Получаем состояние диода в зависимости от его значения (0 или 1). */  
                {           
                    sval = some_state >> (8 - led.pin);
                    sval = sval << (8 - led.pin);
                    some_state &= (127 >> led.pin);
                    some_state += sval;
                    
                }
                else
                   some_state  |= (128 >> led.pin); 

                 printk(KERN_INFO "Hello: Set led state %d %d %d\n", led.pin, led.value, some_state);
                 outb(some_state, PARALLEL_PORT);  /* Запись в принтерный порт некоторого состояния. */
                 retval = 0;
            }
          break;
	    }

        default:{
                printk(KERN_INFO "Hello: Сommand is unsupported\n");
                retval = -EINVAL;
        }
    }

	/* Если команда не поддерживается возвращаем -EINVAL */
	return retval;
}


static void __exit hello_exit(void) /* Деинициализаия */
{
    printk(KERN_INFO "Hello: unregistered\n");
    device_destroy (cl, dev);
    unregister_chrdev_region (dev, 1);
    class_destroy (cl);
}

module_init(hello_init);
module_exit(hello_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Ivan Sidyakin");
MODULE_DESCRIPTION("Simple loadable kernel module");
