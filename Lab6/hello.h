#include "linux/ioctl.h"

struct led_t
{
    unsigned char pin;
    unsigned char value;
};

#define HELLO_MAJOR 100

#define LEDCMD_RESET_STATE _IO(HELLO_MAJOR, 1)                 /* погасить все индикаторы */
#define LEDCMD_GET_STATE _IOR(HELLO_MAJOR, 2, unsigned char *) /* вернуть состояния всех индикаторов */
#define LEDCMD_GET_LED_STATE _IOWR(HELLO_MAJOR, 3,  struct led_t *) /* вернуть значение индикатора с указанным номером */
#define LEDCMD_SET_LED_STATE _IOW(HELLO_MAJOR, 4, struct led_t *) /* установить значение индикатора с указанным номером */ 
